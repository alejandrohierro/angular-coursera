import {Inject, Injectable} from '@angular/core';
import {Promotion} from '../shared/promotion';
import {catchError, map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {ProcessHTTPMsgService} from './process-httpmsg.service';

@Injectable({
  providedIn: 'root'
})
export class PromotionService {

  constructor(private http: HttpClient,
              @Inject('baseURL') private baseURL,
              private processHTTPMsgService: ProcessHTTPMsgService) { }

  getPromotions():  Observable<Promotion[]> {
    return this.http.get<Promotion[]>(this.baseURL + 'promotions')
      .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getPromotion(id: string):  Observable<Promotion> {
    return this.http.get<Promotion>(this.baseURL + 'promotions?id=' + id).pipe(map(promotions => promotions[0]))
      .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getFeaturedPromotion(): Observable<Promotion> {
    return this.http.get<Promotion>(this.baseURL + 'promotions?featured=' + true).pipe(map(promotions => promotions[0]))
      .pipe(catchError(this.processHTTPMsgService.handleError));
  }
}
