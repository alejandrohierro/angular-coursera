import {Inject, Injectable} from '@angular/core';
import {Leader} from '../shared/leader';
import {catchError, map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {ProcessHTTPMsgService} from './process-httpmsg.service';

@Injectable({
  providedIn: 'root'
})
export class LeaderService {

  constructor(private http: HttpClient,
              @Inject('baseURL') private baseURL,
              private processHTTPMsgService: ProcessHTTPMsgService) { }

  getLeaders(): Observable<Leader[]> {
    return this.http.get<Leader[]>(this.baseURL + 'leadership')
      .pipe(catchError(this.processHTTPMsgService.handleError));
  }
  getSpecificLeader(id: string): Observable<Leader> {
    return this.http.get<Leader>(this.baseURL + 'leadership/' + id)
      .pipe(catchError(this.processHTTPMsgService.handleError));
  }
  getFeaturedCorporatedLeaders(): Observable<Leader> {
    return this.http.get<Leader>(this.baseURL + 'leadership?featured=true').pipe(map(leaders => leaders[0]))
      .pipe(catchError(this.processHTTPMsgService.handleError));
  }
}
