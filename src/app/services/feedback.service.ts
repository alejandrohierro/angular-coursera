import {Inject, Injectable} from '@angular/core';
import {Feedback} from '../shared/feedback';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Dish} from '../shared/dish';
import {catchError} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {ProcessHTTPMsgService} from './process-httpmsg.service';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  constructor(private http: HttpClient,
              @Inject('baseURL') private baseURL,
              private processHTTPMsgService: ProcessHTTPMsgService) { }

  submitFeedback(feedback: Feedback): Observable<Feedback>  {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post<Feedback>(this.baseURL + 'feedback', feedback, httpOptions)
      .pipe(catchError(this.processHTTPMsgService.handleError));
  }

}
