import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {Dish} from '../shared/dish';
import {Comment} from '../shared/comment';

import {DishService} from '../services/dish.service';

import {ActivatedRoute, Params} from '@angular/router';
import {Location} from '@angular/common';
import {switchMap} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {visibility} from '../animations/app.animation';
import { flyInOut, expand } from '../animations/app.animation';

@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss']
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    visibility(),
    expand(),
    flyInOut()
  ]
})
export class DishdetailComponent implements OnInit {
  newComment: Comment;
  newCommentForm: FormGroup;
  dishcopy: Dish;
  @ViewChild('cform') commentFormDirective;
  visibility = 'shown';
  formErrors = {
    'author': '',
    'rating': '',
    'comment': ''
  };
  validationMessages = {
    'author': {
      'required': 'Author Name is required.',
      'minlength': 'Author Name must be at least 2 characters long.',
      'maxlength': 'Author Name cannot be more than 50 characters long.'
    },
    'rating': {
      'required': 'Rating is required.'
    },
    'comment': {
      'required': 'Message is required.'
    }
  };
  dish: Dish;
  dishIds: string[];
  prev: string;
  next: string;
  errMess: string;

  constructor(private dishservice: DishService,
              private route: ActivatedRoute,
              private location: Location,
              private fb: FormBuilder,
              @Inject('baseURL') public baseURL) {
    this.createForm();
  }

  createForm() {
    this.newCommentForm = this.fb.group({
      author: ['', [Validators.required, Validators.minLength(2)]],
      rating: [5, [Validators.required]],
      comment: '',
      date: ''
    });
    this.newCommentForm.valueChanges
      .subscribe(data => this.onValueChanged(this.newCommentForm, data));
    this.onValueChanged(this.newCommentForm); // (re)set validation messages now

  }

  onValueChanged(form: FormGroup, data?: any) {
    if (!form) {
      return;
    }
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }

  ngOnInit() {
    this.dishservice.getDishIds().subscribe(dishIds => this.dishIds = dishIds,
      errmess => this.errMess = <any>errmess);
    this.route.params.pipe(switchMap((params: Params) => {
      this.visibility = 'hidden';
      return this.dishservice.getDish(params['id']);
    }))
      .subscribe(dish => {
          this.dish = dish;
          this.dishcopy = dish;
          this.setPrevNext(dish.id);
          this.visibility = 'shown';
        },
        errmess => this.errMess = <any>errmess);
  }

  setPrevNext(dishId: string) {
    const index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length];
  }

  goBack(): void {
    this.location.back();
  }

  onSubmit() {
    this.newComment = this.newCommentForm.value;
    this.newComment.date = new Date().toDateString();
    this.dish.comments.push(this.newComment);
    this.dishservice.putDish(this.dishcopy)
      .subscribe(dish => {
          this.dish = dish;
          this.dishcopy = dish;
        },
        errmess => {
          console.log('sds', errmess);
          this.dish = null;
          this.dishcopy = null;
          this.errMess = <any>errmess;
        });
    this.commentFormDirective.resetForm();
    this.newCommentForm.reset({
      rating: 5,
      comment: '',
      author: '',
      date: '',
    });
  }
}
